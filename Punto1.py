from modulefinder import packagePathMap


matriz = []

def CrearMatriz(nm):
    grid = []
    for x in range(0, int(nm[0])):
        cad = input("Ingrese una cadena: ")
        listaDeCaracteres = []
        for c in cad:
            listaDeCaracteres.append(c)
        grid.append(listaDeCaracteres)
    global matriz 
    matriz = grid

def CrearLista(l):
    lista = []
    for x in range(0, l):
        palabra = input("Escriba la palabra: ")
        lista.append(palabra)
    return lista
                
def BuscarHorizontal(palabra, fila, columna):
    palab = ""
    for x in range(columna, columna+len(palabra)):
        if (len(matriz[fila])>columna+len(palabra)-1):
            palab += matriz[fila][x]
    if (palabra in palab):
        print(str(fila)+", "+str(columna))

def BuscarVertical(palabra, fila, columna):
    palab = ""
    for x in range(fila, fila+len(palabra)):
        if (len(matriz)> fila+len(palabra)-1):
            palab += matriz[x][columna]
    if (palabra in palab):
        print(str(fila)+", "+str(columna))

def BuscarDiagonal(palabra, fila, columna):
    palab = ""
    for x in range(0, len(palabra)):
        if (len(matriz)> fila+len(palabra)-1 and len(matriz[fila])>columna+len(palabra)-1):
            palab += matriz[fila + x][columna + x]
    if (palabra in palab):
        print(str(fila)+", "+str(columna))

def Buscar(palabra):
    for fila in range(0, len(matriz)):
        for columna in range(0, len(matriz[fila])):
            letra = matriz[fila][columna]
            if (letra == palabra[0]):
                BuscarHorizontal(palabra, fila, columna)
                BuscarVertical(palabra, fila, columna)
                BuscarDiagonal(palabra, fila, columna)

nmInput = str(input("Ingrese el numero de filas y numero de columnas de la matriz: ")).split(" ")    
if (len(nmInput) == 2):
    CrearMatriz(nmInput)

    lInput = int(input("Digite el numero de palabras a bucar"))
    lista = CrearLista(lInput)
    for x in lista:
        Buscar(x)
else:
    print("Error")
